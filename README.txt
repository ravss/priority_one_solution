The "android" folder contains the Android mobile application.

The "server" folder contains server-side code like PHP and SQL, along with a test data generator and server stresser as a Python script.

Download/Install Android Studio and then open the "android" directory in Android Studio to begin work.
Also enable VT-x or SVM in your UEFI/BIOS to help increase QEMU's performance as it emulates an Android device.

You must configure the file "./server/database_credentials.ini" and then place it outside your web root so it
is inaccessible by internet users. Once that is done, you must configure the PHP script variable called "$secrets_file" to point to it.

Afterwards, edit "./android/app/src/main/res/values/priority_one_config.xml" and set the appropriate values to reflect your
configuration, particularly the server root, the server path, and the Google API key.

There are inline comments throughout the code which explain what needs to be done and how the program functions. There should be no issue comprehending the code if you are familiar with Java Android APIs, PHP, and SQL.

Finally, compile the mobile application and begin usage or further development.