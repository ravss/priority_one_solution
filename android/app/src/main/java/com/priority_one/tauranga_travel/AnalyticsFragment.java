package com.priority_one.tauranga_travel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.List;

// This class is a fragment that could be used to add in more analytics. For now, we've just
// done average speed as a simple demonstration. Future ideas may include distance travelled etc.
public class AnalyticsFragment extends Fragment {
    public static List<Float> speeds;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment.
        return inflater.inflate(R.layout.fragment_analytics, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        speeds = new ArrayList<>();
    }
}
