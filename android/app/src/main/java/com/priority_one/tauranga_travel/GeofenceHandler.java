package com.priority_one.tauranga_travel;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofenceStatusCodes;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

// This is where you should handle geofence transition events. The possibility is limitless
// here for detection of user movement and if they want to travel or not etc.
public class GeofenceHandler extends BroadcastReceiver {
    private static final String tag = "PriorityOneGeofenceLog";

    // TODO: This does not account for situations where the geofences overlap. It may work fine,
    // but it will not warn the user about any oddities.
    public void onReceive(Context context, Intent intent) {
        Log.i(tag, "Received geofence event.");
        final GeofencingEvent event = GeofencingEvent.fromIntent(intent);

        if (event.hasError()) {
            final String error = GeofenceStatusCodes.getStatusCodeString(event.getErrorCode());
            Log.e(tag, "Geofencing event has an error - '" + error + "'.");
            return;
        }

        final String homeAddress = TravelActivity.homeMarker.getSnippet();
        final String workAddress = TravelActivity.workMarker.getSnippet();
        final int transition = event.getGeofenceTransition();
        final List<Geofence> triggers = event.getTriggeringGeofences();

        if (transition == Geofence.GEOFENCE_TRANSITION_DWELL) { // Arriving at the destination.
            Log.i(tag, "Entered a geofence.");
            TravelActivity.arrived = true;

            if (triggers.get(0).getRequestId().equals(TravelActivity.homeName)) {
                if (TravelActivity.destination == TravelActivity.AreaType.home) {
                    TravelActivity.destination = TravelActivity.AreaType.work;
                    Log.i(tag, "User has arrived at '" + homeAddress + "'.");
                }
            }
            else if (triggers.get(0).getRequestId().equals(TravelActivity.workName)) {
                if (TravelActivity.destination == TravelActivity.AreaType.work) {
                    TravelActivity.destination = TravelActivity.AreaType.home;
                    Log.i(tag, "User has arrived at '" + workAddress + "'.");
                }
            }
            else {
                Log.e(tag, "Geofence dwell transition on unknown event.");
            }
        }
        else if (transition == Geofence.GEOFENCE_TRANSITION_EXIT) { // Leaving the origin.
            Log.i(tag, "Left a geofence.");
            TravelActivity.arrived = false;

            if (triggers.get(0).getRequestId().equals(TravelActivity.homeName)) {
                if (TravelActivity.destination == TravelActivity.AreaType.work) {
                    Log.i(tag, "User has left from '" + homeAddress + "'.");
                }
            }
            else if (triggers.get(0).getRequestId().equals(TravelActivity.workName)) {
                if (TravelActivity.destination == TravelActivity.AreaType.home) {
                    Log.i(tag, "User has left from '" + workAddress + "'.");
                }
            }
            else {
                Log.e(tag, "Geofence exit transition on unknown event.");
                return;
            }

            TravelActivity.waitingToBegin = true;
        }
    }
}
