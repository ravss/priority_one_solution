package com.priority_one.tauranga_travel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.util.Log;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.maps.android.PolyUtil;

import org.jetbrains.annotations.Contract;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.google.android.gms.location.GeofencingRequest.INITIAL_TRIGGER_DWELL;
import static com.google.android.gms.location.LocationServices.getFusedLocationProviderClient;
import static com.google.android.gms.maps.model.JointType.ROUND;

// This is the activity for travelling. It contains a large amount of code all related to configuring
// the map (setting up markers, routers, areas) and then handling the travel events and location data.
// For the convenience of future experimentation, there are a lot of class-wide variables for tweaking things.
// TODO: This entire class needs to be refactored, it has become large. Need a new Volley requests class.
public class TravelActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String tag = "PriorityOneTravelLog";
    private GoogleMap map;
    private NavigationView navigationView;
    private LocationRequest locationRequest; // Main location request.
    private RequestQueue locationQueue; // TODO: Make this into a singleton class as said above.
    private Geocoder geocoder;
    private GeofencingClient geofencer;
    private PendingIntent geofencePendingIntent = null;
    private GeofencingRequest.Builder travelEvent;

    // These determine if the program is going to send specific data or not.
    private static volatile Boolean online = true;
    public static volatile Boolean travelling = false; // Default state.
    public static volatile Boolean arrived = !travelling;
    public static volatile Boolean waitingToBegin = false;
    public static volatile Boolean firstTime = true;
    public static volatile Boolean showUserRoute = true;

    // Some camera settings. This chooses whether or not the camera will stick to the user.
    private static volatile Boolean stickyCamera = false;

    // Request tags for Volley.
    private static final String dataRequestTag = "LocationDataRequest";
    private static final String journeyRequestTag = "JourneyRequest";

    // Since this is a mobile app, connections can be weak. Tweak these if needed for HTTPS retries.
    private static int retryTimeout = 3000; // Two seconds lower than the Volley default for now.
    private static int maxRetries = 10; // The default retry limit (five) times two.
    private RetryPolicy customRetryPolicy;

    // Sensitivity settings. Configure as needed. TODO: Make functions to tune and set these.
    private static volatile long deltaMs = 4000; // Milliseconds between each data push.
    private static volatile long deltaMsFast = 1500; // The fastest possible update (e.g. user is in a car).
    private static volatile float minMoveMetre = 6.00f; // A minimum of 6 metres for a location update to occur.

    // This will contain their preset location settings.
    private SharedPreferences preferences;

    // These control the markers/areas and if they are set or not. By default, they are not set.
    public static List<Marker> mapMarkers = new ArrayList<>();
    public static final String homeName = "Home";
    public static final String workName = "Workplace";
    public static MarkerOptions homeMarker;
    public static MarkerOptions workMarker;
    private static Geofence.Builder homeArea;
    private static Geofence.Builder workArea;
    private static Circle homeShape;
    private static Circle workShape;
    private static Polyline travelRoute;
    private static Polyline userRoute;
    private static volatile Boolean homeSet = false;
    private static volatile Boolean workSet = false;

    // These indicate what type of area something belongs to.
    public enum AreaType {home, work}
    public static AreaType destination = null;

    // Increasing this value makes the geofence larger (radius in metres). Exiting it starts travel.
    // Google recommends a 100 metre radius as a minimum.
    // TODO: Perhaps make this user configurable in the future.
    private static final float areaRadius = 100.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(tag,"Main travelling screen reached.");

        // This should occur after the user has logged on.
        setContentView(R.layout.activity_travel);
        setTitle("Mapping"); // Default menu that shows on startup.

        // The main views and fragments.
        Toolbar mainToolbar = (Toolbar) findViewById(R.id.toolbar);
        DrawerLayout mainDrawer = (DrawerLayout) findViewById(R.id.drawer);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        // Set up the toolbar as the main one and then set the navigation view for the navbar/drawer.
        setSupportActionBar(mainToolbar);
        navigationView = findViewById(R.id.navbar);

        // Enable the "hamburger" navbar toggle.
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, mainDrawer, mainToolbar, R.string.navbar_open, R.string.navbar_close);
        mainDrawer.addDrawerListener(toggle);
        toggle.setDrawerIndicatorEnabled(true);
        toggle.syncState();

        // Create only one request queue for the travelling activity and one object for preferences.
        locationQueue = Volley.newRequestQueue(this);
        geocoder = new Geocoder(this, Locale.ENGLISH);
        geofencer = LocationServices.getGeofencingClient(this);
        preferences = getSharedPreferences(SignInActivity.preferencesName, MODE_PRIVATE);

        // Prepare the map. If something went wrong with the API itself somehow, then tell the user.
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        else {
            Log.e(tag, "Severe internal error with Google Maps API.");
            Toast.makeText(this, "Google Maps API is unavailable.", Toast.LENGTH_LONG).show();
            throw new RuntimeException();
        }

        // Set up the location processor (along with the main loop) and the retry policy.
        customRetryPolicy = new DefaultRetryPolicy(retryTimeout, maxRetries, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        locationCollection();
    }

    // This does not do anything of true use for now, but this is where development would begin
    // to handle the Android lifecycle more fluently.
    @Override
    protected void onStart() {
        super.onStart();
        locationQueue.start();
    }

    // Pressing the Android back button simply signs the user out.
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // Right now, this ends the tracking if the user presses the Android back button.

        if (travelling) {
            journeyManager(true, true, null, null);
        }

        Toast.makeText(this, "Signed out, tracking has stopped.", Toast.LENGTH_LONG).show();
    }

    // If the application stops, then it will also pause the request queue. This is out of scope.
    @Override
    protected void onStop() {
        super.onStop();
        locationQueue.stop();
    }

    // This contains the main loop of the program and decides whether to start travelling or not.
    private void mainLoop(LocationResult locationResult) {
        if (travelling) {
            if (arrived) { // TODO: Potential race condition here.
                journeyManager(true, false, null, null);
            }
            else {
                locationProcessor(locationResult.getLastLocation());
            }
        }
        else if (homeSet && workSet && waitingToBegin) {
            waitingToBegin = false;

            if (destination == AreaType.home) {
                if (journeyManager(false, false, workMarker.getSnippet(), homeMarker.getSnippet())) {
                    drawRoute(false, workMarker.getPosition(), homeMarker.getPosition());
                }
            }
            else {
                if (journeyManager(false, false, homeMarker.getSnippet(), workMarker.getSnippet())) {
                    drawRoute(false, homeMarker.getPosition(), workMarker.getPosition());
                }
            }
        }

        // For now, the custom button acts as a camera adjuster, but in the future, it can be customised
        // to do other things. This is just an example.
        if (stickyCamera) {
            final Location lastUserLocation = locationResult.getLastLocation();
            final LatLng userLocationCamera = new LatLng(lastUserLocation.getLatitude(), lastUserLocation.getLongitude());
            map.animateCamera(CameraUpdateFactory.newLatLng(userLocationCamera));
        }
    }

    // Handles the navigation drawer interaction. 
    public void switchNavItem(@NonNull final MenuItem item) {
        final FragmentManager fragmentManager = getSupportFragmentManager();
        final Fragment mapFragment = fragmentManager.findFragmentById(R.id.map);
        final Fragment journeysFragment = fragmentManager.findFragmentById(R.id.journeys);
        final Fragment analyticsFragment = fragmentManager.findFragmentById(R.id.analytics);

        if (mapFragment == null || journeysFragment == null || analyticsFragment == null) {
            Log.e(tag, "A fragment does not exist and is null.");
            return;
        }

        final FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);

        // TODO: This could maybe benefit from `addToBackStack` etc. for the fragment transaction
        // so the menu switches are more natural.
        switch (item.getItemId()) {
            case R.id.nav_mapping:
                Log.i(tag, "Now showing map.");
                fragmentTransaction.show(mapFragment);
                fragmentTransaction.hide(journeysFragment);
                fragmentTransaction.hide(analyticsFragment);
                break;
            case R.id.nav_journeys:
                Log.i(tag, "Now showing previous journeys.");
                fragmentTransaction.hide(mapFragment);
                fragmentTransaction.show(journeysFragment);
                fragmentTransaction.hide(analyticsFragment);
                break;
            case R.id.nav_analytics:
                Log.i(tag, "Now showing current analytics.");
                fragmentTransaction.hide(mapFragment);
                fragmentTransaction.hide(journeysFragment);
                fragmentTransaction.show(analyticsFragment);
                break;
            default:
                Log.e(tag, "Unknown navbar item selected - ID:" + item.getItemId() + '.');
                return;
        }

        fragmentTransaction.commit();
        fragmentManager.executePendingTransactions();

        setTitle(item.getTitle());
        ((DrawerLayout) findViewById(R.id.drawer)).closeDrawers();
    }

    // Gets a pending intent for usage with the geofence handler.
    private PendingIntent getGeofencePendingIntent() {
        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }

        final Intent intent = new Intent(this, GeofenceHandler.class);
        geofencePendingIntent = PendingIntent.getBroadcast(this, 0, intent , PendingIntent.FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }

    // Once all the markers are indeed set, this creates an actual area around them that
    // listens for user movement e.g. the user leaves an area and then the application can
    // do something about it.
    private void setNewGeofences() {
        Log.i(tag, "Trying to add new geofences.");

        travelEvent.setInitialTrigger(INITIAL_TRIGGER_DWELL);

        geofencer.addGeofences(travelEvent.build(), getGeofencePendingIntent()).addOnSuccessListener(this, new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void empty) {
                Log.i(tag, "Added geofences.");
            }
        }).addOnFailureListener(this, new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception error) {
                Log.e(tag, "Failed to add geofences - " + error.getLocalizedMessage() + '_' + error.getMessage());
                if (error.getMessage() == null) {
                    return;
                }
                else if (error.getMessage().equals("1000: ")) {
                    Toast.makeText(TravelActivity.this, "Enable 'High accuracy' in your Location settings first.", Toast.LENGTH_LONG).show();
                }
                else if (error.getMessage().equals("13: ")) {
                    // This error occurs with Android API 29 sometimes, it's an issue on Google's end.
                    Toast.makeText(TravelActivity.this, "Internal Google Play Services/API error, update your device.", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // This is an example of asking the user as opposed to doing it automatically.
    private void prepareNewGeofences() {
        // TODO: In a future update, this needs to be replaced with a Dialog Fragment.
        AlertDialog.Builder initialLocation = new AlertDialog.Builder(this);
        initialLocation.setMessage("Where are you going to first?");
        initialLocation.setCancelable(true);

        initialLocation.setPositiveButton("Home", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                destination = AreaType.home;
                setNewGeofences();
                dialog.cancel();
            }
        });

        initialLocation.setNegativeButton("Work", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                destination = AreaType.work;
                setNewGeofences();
                dialog.cancel();
            }
        });

        initialLocation.show();
    }

    // Set up the location request object and customise things like intervals.
    private void locationCollection() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(deltaMs);
        locationRequest.setFastestInterval(deltaMsFast);
        locationRequest.setSmallestDisplacement(minMoveMetre);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Create the locationSettings object with the above configured request.
        final LocationSettingsRequest.Builder locationSettingsBuilder = new LocationSettingsRequest.Builder();
        locationSettingsBuilder.addLocationRequest(locationRequest);
        LocationSettingsRequest locationSettings = locationSettingsBuilder.build();

        // Check if we are good to go.
        final SettingsClient settingsClient = LocationServices.getSettingsClient(this);
        settingsClient.checkLocationSettings(locationSettings).addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(@NonNull Task<LocationSettingsResponse> task) {
                try {
                    final LocationSettingsResponse response = task.getResult(ApiException.class);
                    getFusedLocationProviderClient(TravelActivity.this).requestLocationUpdates(locationRequest, new LocationCallback() {
                        @Override
                        public void onLocationResult(LocationResult locationResult) {
                            mainLoop(locationResult);
                        }
                    }, Looper.myLooper()); // Begin the Looper thread on its own thread instead of the main thread.
                }
                catch (ApiException error) {
                    // TODO: Stronger error checking needs to be added here if the mandatory check fails or needs a resolution.
                    Log.e(tag, "User location settings are inadequate: ApiException - " + error.getStatusCode() + '.');

                    AlertDialog.Builder locationError = new AlertDialog.Builder(TravelActivity.this);
                    locationError.setTitle("Location Error");
                    locationError.setMessage("Enable 'High accuracy' in your Location settings first or else your location is unobtainable.");
                    locationError.setCancelable(false);

                    locationError.setNeutralButton("Exit", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.cancel();
                        }
                    });

                    locationError.show();
                }
            }
        });
    }

    // Do address display formatting here. For now, it just gets the closest address result.
    private String getAddress(final double latitude, final double longitude) {
        Log.i(tag, "Address requested.");
        String locatedAddress = "< Could Not Get Street >";
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses == null || addresses.size() < 1) {
                return locatedAddress;
            }
        }
        catch (Exception ioError) { // IOFileException
            return locatedAddress;
        }

        locatedAddress = addresses.get(0).getSubThoroughfare() != null ? addresses.get(0).getSubThoroughfare() + ' ' : "";
        locatedAddress += addresses.get(0).getThoroughfare() != null ? addresses.get(0).getThoroughfare() : "";

        Log.i(tag, "Address provided is '" + locatedAddress + "'.");
        return locatedAddress.isEmpty() ? "< No Address >" : locatedAddress;
    }

    // Handles the interface by taking parameters in where the user clicked on the map and then
    // reflects it by adding a marker, along with saving its location to the activities settings.
    @NonNull
    private Boolean setupMarker(@NonNull final MarkerOptions marker, @NonNull final Geofence.Builder fence, final int button,
                                @NonNull Circle shape, final LatLng location, @NonNull final AreaType type)
    {
        final SharedPreferences.Editor editor = preferences.edit();

        findViewById(button).setVisibility(View.INVISIBLE);

        marker.position(location);
        marker.snippet(getAddress(location.latitude, location.longitude));
        mapMarkers.add(map.addMarker(marker));

        fence.setCircularRegion(location.latitude, location.longitude, areaRadius);
        travelEvent.addGeofence(fence.build());

        shape.setCenter(location);
        shape.setVisible(true);

        editor.putString(type.name() + "Latitude", String.format(Locale.ENGLISH, "%.6f", location.latitude));
        editor.putString(type.name() + "Longitude", String.format(Locale.ENGLISH, "%.6f", location.longitude));
        editor.apply();
        return true;
    }

    // For now, both geofences (home and work) are identical in properties. Letting the user
    // customise their area and delay etc. is out of this prototype's scope.
    private Geofence.Builder newGeofenceBuilder() {
        Geofence.Builder newFence = new Geofence.Builder();
        newFence.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_DWELL | Geofence.GEOFENCE_TRANSITION_EXIT);
        newFence.setLoiteringDelay(3500);
        newFence.setExpirationDuration(Geofence.NEVER_EXPIRE);
        newFence.setNotificationResponsiveness(2000); // Setting this too low wastes power.
        return newFence;
    }

    // This returns a circle object that has been added to the map. It represents the geofence
    // for the user. It is only for graphical purposes and does not serve any functionality outside of that.
    @NonNull
    private Circle newGeofenceCircle(@NonNull final AreaType type) {
        final CircleOptions circle = new CircleOptions();
        final int homeColour = Color.argb(30, 0, 0, 255);
        final int workColour = Color.argb(30, 255, 0, 0);

        circle.radius(areaRadius);
        circle.visible(false);
        circle.fillColor(type == AreaType.home ? homeColour : workColour);
        circle.strokeColor(Color.TRANSPARENT);

        // No location until `markerSetup()` sets it.
        circle.center(new LatLng(0, 0));

        return map.addCircle(circle);
    }

    // Sets up the travel activity. Also retrieves previous settings etc.
    private void travelSetup() {
        travelEvent = new GeofencingRequest.Builder();
        homeMarker = new MarkerOptions();
        workMarker = new MarkerOptions();
        homeArea = newGeofenceBuilder();
        workArea = newGeofenceBuilder();
        homeShape = newGeofenceCircle(AreaType.home);
        workShape = newGeofenceCircle(AreaType.work);

        // First, let's set up the marker options.
        homeMarker.title(homeName);
        workMarker.title(workName);
        homeArea.setRequestId(homeName);
        workArea.setRequestId(workName);
        homeShape.setTag(homeName);
        workShape.setTag(workName);

        homeMarker.draggable(true);
        workMarker.draggable(true);

        homeMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        workMarker.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));

        final String homeLatitude = preferences.getString("homeLatitude", null);
        final String homeLongitude= preferences.getString("homeLongitude", null);
        final String workLatitude = preferences.getString("workLatitude", null);
        final String workLongitude = preferences.getString("workLongitude", null);

        if (homeLatitude != null && homeLongitude != null && !homeLatitude.isEmpty() && !homeLongitude.isEmpty()) {
            final LatLng location = new LatLng(Double.parseDouble(homeLatitude), Double.parseDouble(homeLongitude));
            homeSet = setupMarker(homeMarker, homeArea, R.id.home_button, homeShape, location, AreaType.home);
        }

        if (workLatitude != null && workLongitude != null && !workLatitude.isEmpty() && !workLongitude.isEmpty()) {
            final LatLng location = new LatLng(Double.parseDouble(workLatitude), Double.parseDouble(workLongitude));
            workSet = setupMarker(workMarker, workArea, R.id.work_button, workShape, location, AreaType.work);
        }

        // TODO: You can either ask the user every time or assume they're at one of the locations.
        if (homeSet && workSet) {
            setNewGeofences();
        }

        // Finally, recall the user's setting for the sticky camera.
        if (preferences.getBoolean("stickyCamera", false)) {
            stickyCamera = true;
            ((Button) findViewById(R.id.stick_button)).setText(getResources().getString(R.string.sticky_camera_enabled));
        }
    }

    // Handles the 3-vertical-dot menu on the top-right corner.
    public void overflowMenu(@NonNull final View mapView) {
        final PopupMenu popup = new PopupMenu(this, mapView);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.reset_markers) {
                    final SharedPreferences.Editor editor = preferences.edit();

                    if (travelling) {
                        journeyManager(true, true, null, null);
                    }

                    homeSet = false;
                    workSet = false;
                    homeShape.setVisible(homeSet);
                    workShape.setVisible(workSet);

                    for (final Marker marker : mapMarkers) {
                        marker.remove();
                    }

                    mapMarkers.clear();

                    editor.remove("homeLatitude").remove("homeLongitude");
                    editor.remove("workLatitude").remove("workLongitude");

                    findViewById(R.id.home_button).setVisibility(View.VISIBLE);
                    findViewById(R.id.work_button).setVisibility(View.VISIBLE);

                    geofencer.removeGeofences(getGeofencePendingIntent());
                    travelEvent = new GeofencingRequest.Builder();

                    firstTime = true;
                    editor.apply();

                    Log.i(tag, "User reset markers.");
                    Toast.makeText(TravelActivity.this, "Markers and areas resetted.", Toast.LENGTH_SHORT).show();
                    return true;
                }
                else if (item.getItemId() == R.id.help) {
                    Log.i(tag, "User asked for help.");
                    final String helpMessage = "To begin, set the two areas for both your home " +
                            "and workplace. After you have done that, open up the application any " +
                            "time you wish to travel to work or home from either location.";

                    final AlertDialog.Builder help = new AlertDialog.Builder(TravelActivity.this);
                    help.setTitle("Help").setMessage(helpMessage).setCancelable(true);

                    help.setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    }).show();

                    return true;
                }
                else {
                    return false;
                }
            }
        });

        popup.inflate(R.menu.right_popup);
        popup.show();
    }

    // Processes the location data for e.g. analytics, then it calls the function to send it to the server.
    protected void locationProcessor(@NonNull final Location update) {
        final String latitude = Double.toString(update.getLatitude());
        final String longitude = Double.toString(update.getLongitude());
        final String speed = Float.toString(update.getSpeed());
        final String time = Long.toString(update.getTime()); // 64-bit Unix epoch/timestamp in milliseconds.
        final String movement;

        // TODO: Add some speed detection stuff here, stand-by heuristics, really everything
        // to do with how we're going to further make the app more useful. That is outside the scope
        // of this very prototype; however, here's a quick example with very rough numbers:
        final float metresPerSecond = update.getSpeed();
        if (metresPerSecond < 0.4) {
            movement = "Stationary"; // Slight movement of a device can still mean the user is standing.
        }
        else if (metresPerSecond > 0.4 && metresPerSecond < 7.5) {
            movement = "Walking"; // Covers running or sprinting.
        }
        else if (metresPerSecond > 7.5) {
            movement = "Vehicle";
        }
        else {
            movement = "Unknown";
        }

        // This is for the analytics fragment. Again, this is just a small example.
        // TODO: Shift the update to the fragment itself on its own thread etc.
        final TextView analyticsSpeedView = findViewById(R.id.analytics_speed);
        Float speedSum = 0f;
        AnalyticsFragment.speeds.add(metresPerSecond);

        for (Float speedValue : AnalyticsFragment.speeds) {
            speedSum += speedValue;
        }

        final String analyticsSpeedText = String.format(Locale.ENGLISH, "Average speed: %.6f m/s",
                speedSum / AnalyticsFragment.speeds.size());
        analyticsSpeedView.setText(analyticsSpeedText);;

        final String example = "La:" + latitude + " Lo:" + longitude + " S:" + speed + " T:" + time + " M:" + movement;
        Log.i(tag, example);

        if (showUserRoute) {
            final LatLng userLocation = new LatLng(update.getLatitude(), update.getLongitude());
            final LatLng destinationLocation = destination == AreaType.home ? homeMarker.getPosition() : workMarker.getPosition();
            drawRoute(true, userLocation, destinationLocation);
        }

        locationSender(latitude, longitude, speed, time, movement);
    }

    // Returns a bool while also updating the global class-wide online variable.
    private Boolean checkConnection() {
        final ConnectivityManager connection = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo network = connection != null ? connection.getActiveNetworkInfo() : null;
        online = network != null && network.isConnected();
        return online;
    }

    // This function sends the data to the server, all fields must be present.
    private void locationSender(final String latitude, final String longitude, final String speed, final String time, final String movement) {
        // For now, I don't think JSON is necessary, so let's just use POST data again.
        StringRequest dataPush = new StringRequest(Request.Method.POST, SignInActivity.server + SignInActivity.path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("1")) {
                    Log.i(tag, "Successful data push.");
                }
                else if (response.equals("0")) {
                    Log.e(tag, "Server responded with error.");
                    Toast.makeText(TravelActivity.this, "Server rejected location data.", Toast.LENGTH_LONG).show();
                }
                else { // This occurs when something really goes wrong.
                    Log.e(tag, "Server response unrecognised, unexpected server error - '" + response + "'.");
                    Toast.makeText(TravelActivity.this, "The server is currently experiencing issues.", Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Need a much better way to deal with a network state issue.
                Log.e(tag, "Connection error detected - '" + error.getMessage() + "'.");

                // `NoConnectionError` is covered by `NetworkError`, which Volley can throw.
                if (error instanceof NetworkError) {
                    Toast.makeText(TravelActivity.this, "Network error, please check your internet connection.", Toast.LENGTH_LONG).show();
                    networkErrorHandler();
                }
            }
        }){
            @NonNull @Contract(" -> new") @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    put("Cookie", SignInActivity.session);
                }};
            }

            @NonNull @Contract(" -> new") @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    put("location", ""); // Just need to set this parameter, no data needed.
                    put("latitude", latitude);
                    put("longitude", longitude);
                    put("speed", speed);
                    put("movement", movement);
                    put("unixtimestampms", time);
                }};
            }
        };

        dataPush.setTag(dataRequestTag);
        locationQueue.add(dataPush);
    }

    // Gets a polyline route from the Google Directions API and draws it onto the map.
    private void drawRoute(@NonNull final Boolean userLocation, @NonNull final LatLng startLocation, @NonNull final LatLng endLocation) {
        Log.i(tag, "Drawing route at " + startLocation.toString() + " to " + endLocation.toString());

        String getRoute = "https://maps.googleapis.com/maps/api/directions/json";
        getRoute += "?origin=" + String.format(Locale.ENGLISH, "%.6f,%.6f", startLocation.latitude, startLocation.longitude);
        getRoute += "&destination=" + String.format(Locale.ENGLISH, "%.6f,%.6f", endLocation.latitude, endLocation.longitude);
        getRoute += "&key=" + this.getString(R.string.google_maps_key);

        locationQueue.add(new StringRequest(Request.Method.GET, getRoute, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    final PolylineOptions newRoute = new PolylineOptions();
                    final JSONObject parsedResponse = new JSONObject(response);

                    final JSONObject routes = parsedResponse.getJSONArray("routes").getJSONObject(0);

                    // TODO: You need to pick which type of route you want e.g. walking or driving.
                    // The below is an example of walking. This is outside our scope.
                    /* final JSONObject legs = routes.getJSONArray("legs").getJSONObject(0);
                    final JSONObject steps = legs.getJSONArray("steps").getJSONObject(0);
                    final String points = steps.getJSONObject("polyline").getString("points"); */

                    // This is just using the overview polyline, it's less detailed but okay for the prototype.
                    final String points = routes.getJSONObject("overview_polyline").getString("points");

                    /* Log.i(tag, "Encoded polyline - " + points); */
                    newRoute.addAll(PolyUtil.decode(points));
                    newRoute.width(15); // 10 is the default width.
                    newRoute.jointType(ROUND);

                    if (userLocation) {
                        newRoute.color(Color.argb(130, 0, 255, 0));
                        newRoute.zIndex(10); // Put it above every other polyline on its layer.

                        if (userRoute != null) {
                            userRoute.remove();
                        }

                        userRoute = map.addPolyline(newRoute);
                    }
                    else {
                        newRoute.color(Color.argb(130, 255, 165, 0));
                        newRoute.zIndex(1); // Put it below every other polyline on its layer.
                        travelRoute = map.addPolyline(newRoute);
                    }
                }
                catch (Exception error) {
                    Log.e(tag, "Route drawer failed.");
                    if (error instanceof JSONException) {
                        Toast.makeText(TravelActivity.this, "Not possible to draw the route.", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(TravelActivity.this, "Failed to draw the route.", Toast.LENGTH_LONG).show();
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(TravelActivity.this, "Could not draw the route.", Toast.LENGTH_LONG).show();
            }
        }));
    }

    // This should be called once the user exits their work or home address bounds.
    // Only a single journey is going to occur at once and the server stores the rest, so I've avoided overdoing
    // OOP and have refrained from making a new class altogether with the goal of keeping it simple.
    // There's two ways to end a journey, either properly by supply false as the second argument or vice versa.
    @NonNull
    private Boolean journeyManager(final Boolean endingJourney, final Boolean cancelledEnding,
                                   @Nullable final String startAddress, @Nullable final String endAddress)
    {
        if (travelRoute != null) {
            travelRoute.remove();
        }

        if (userRoute != null) {
            userRoute.remove();
        }

        final StringRequest journeyPush = new StringRequest(Request.Method.POST, SignInActivity.server + SignInActivity.path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response.equals("1")) {
                    if (!endingJourney) {
                        Log.i(tag, "New journey record created in database.");
                        Toast.makeText(TravelActivity.this, "Beginning travel...", Toast.LENGTH_LONG).show();
                        travelling = true;
                    }
                    else if (!cancelledEnding){
                        Log.i(tag, "Journey record updated and finalised in database.");
                        Toast.makeText(TravelActivity.this, "You have arrived.", Toast.LENGTH_LONG).show();
                        travelling = false;
                    }
                    else {
                        Log.w(tag, "Journey record incomplete due to cancellation.");
                        Toast.makeText(TravelActivity.this, "Journey cancelled.", Toast.LENGTH_LONG).show();
                        travelling = false;
                    }
                }
                else if (response.equals("0")) {
                    Log.e(tag, "Server responded with error.");
                    Toast.makeText(TravelActivity.this, "Could not track your journey.", Toast.LENGTH_LONG).show();
                    travelling = false;
                }
                else { // This occurs when something really goes wrong.
                    Log.e(tag, "Server response unrecognised, unexpected server error - '" + response + "'.");
                    Toast.makeText(TravelActivity.this, "The server is currently experiencing issues.", Toast.LENGTH_LONG).show();
                    travelling = false;
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Need a much better way to deal with a network state issue.
                Log.e(tag, "Journey starter connection error detected - '" + error.getMessage() + "'.");

                // `NoConnectionError` is covered by `NetworkError`, which Volley can throw.
                if (error instanceof NetworkError) {
                    Toast.makeText(TravelActivity.this, "Network error, please check your internet connection.", Toast.LENGTH_LONG).show();
                    networkErrorHandler();
                }
                else if (error instanceof AuthFailureError) {
                    // TODO: If for some reason the user's session times out, then we must refresh/create a new one. Handle this later.
                    Toast.makeText(TravelActivity.this, "Authorisation error, please exit the application and try again.", Toast.LENGTH_LONG).show();
                }
            }
        }){
            @NonNull @Contract(" -> new") @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    put("Cookie", SignInActivity.session);
                }};
            }

            @NonNull @Contract(" -> new") @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    if (!endingJourney) {
                        put("journey", "start");
                        put("startaddress", startAddress);
                        put("endaddress", endAddress);
                    }
                    else {
                        put("journey", cancelledEnding ? "cancel" : "end");
                    }
                }};
              }
        };

        locationQueue.cancelAll(dataRequestTag); // Cancel any spurious data requests.
        journeyPush.setTag(journeyRequestTag);
        journeyPush.setRetryPolicy(customRetryPolicy);
        AnalyticsFragment.speeds.clear();

        // TODO: This is another example of connection status handling. It's out of the prototype's scope.
        if (checkConnection()) {
            locationQueue.add(journeyPush);
            return true;
        }
        else {
            return false;
        }
    }

    // TODO: This is not a reliable method of handling the network state at all,
    // it's just a placeholder for later development.
    private void networkErrorHandler() {
        if (checkConnection()) {
            locationQueue.start();
        }
        else {
            locationQueue.stop();
        }
    }

    // Controls where the areas on the map are set by the user during interaction.
    public void manageArea(@NonNull final View mapView) {
        final AreaType type = mapView.getTag().toString().equals("setHome") ? AreaType.home : AreaType.work;

        final GoogleMap.OnMapClickListener mapListener = new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng clickedLocation) {
                map.setOnMapClickListener(null);

                // Cancel any previous journeys.
                if (travelling) {
                    journeyManager(true, true, null, null);
                }

                if (type == AreaType.home) {
                    homeSet = setupMarker(homeMarker, homeArea, R.id.home_button, homeShape, clickedLocation, type);
                }
                else {
                    workSet = setupMarker(workMarker, workArea, R.id.work_button, workShape, clickedLocation, type);
                }

                if (homeSet && workSet) {
                    prepareNewGeofences();
                    return;
                }

                map.animateCamera(CameraUpdateFactory.newLatLng(clickedLocation));
            }
        };

        Log.i(tag, "User clicked " + type + " setting button.");
        Toast.makeText(this, "Tap on your " + type + "'s map location.", Toast.LENGTH_SHORT).show();
        map.setOnMapClickListener(mapListener);
    }

    // This is just an example of what can be bound to a custom button, right now it makes the camera
    // stick to the user's location.
    public void setStickyCamera(@NonNull final View mapView) {
        // The main variable is volatile, can't do `stickyCamera = !stickCamera`.
        if (stickyCamera) {
            stickyCamera = false;
            Log.i(tag, "Sticky camera disabled.");
            ((Button)findViewById(R.id.stick_button)).setText(getResources().getString(R.string.sticky_camera_disabled));
            Toast.makeText(this, "Automatic camera location follow disabled.", Toast.LENGTH_SHORT).show();
        }
        else if (!stickyCamera) {
            stickyCamera = true;
            Log.i(tag, "Sticky camera enabled.");
            ((Button)findViewById(R.id.stick_button)).setText(getResources().getString(R.string.sticky_camera_enabled));
            Toast.makeText(this, "Automatic camera location follow enabled.", Toast.LENGTH_SHORT).show();
        }

        final SharedPreferences.Editor editor = preferences.edit().putBoolean("stickyCamera", stickyCamera);
        editor.apply();
    }

    // A simple toggle for Google's traffic data layer on the map itself. Not accurate.
    public void trafficSwitch(@NonNull final View trafficSwitch) {
        final boolean showTraffic = ((SwitchCompat) trafficSwitch).isChecked();
        map.setTrafficEnabled(showTraffic);
        Log.i(tag, "Toggled traffic to " + showTraffic + '.');
    }

    // Another toggle that controls whether a route from the user's current location should occur.
    // This can cause a lot more data usage, so the user can disable it if they're just going to
    // follow the marker-to-marker route.
    public void userRouteSwitch(@NonNull final View userRouteSwitch) {
        showUserRoute = ((SwitchCompat) userRouteSwitch).isChecked();

        if (!showUserRoute && userRoute != null) {
            userRoute.remove();
        }

        Log.i(tag, "Toggled show user route to " + showUserRoute + '.');
    }

    // Once the map is ready to go, a few more setups and initiations occur.
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        final UiSettings mapSettings = map.getUiSettings();

        // This is a quick bound line I made across Tauranga. Not sure what areas to include.
        final LatLngBounds tauranga = new LatLngBounds(
                new LatLng(-37.763735, 176.029676),
                new LatLng(-37.605877, 176.293692));
        final float defaultCameraZoom = 11f;

        // Marker example.
        /* LatLng UoW = new LatLng(-37.685093, 176.166992);
        map.addMarker(new MarkerOptions().position(UoW).title("University of Waikato Tauranga Campus")); */

        map.setLatLngBoundsForCameraTarget(tauranga);
        map.setMinZoomPreference(defaultCameraZoom);
        map.moveCamera(CameraUpdateFactory.newLatLng(tauranga.getCenter()));
        map.setMyLocationEnabled(true);

        // Using Google's traffic data can be toggled later on.
        map.setTrafficEnabled(false);

        // Change these as you like. We've just enabled all of them except for the
        // My Location button, where we have provided our own for any additional customisation.
        mapSettings.setMyLocationButtonEnabled(false);
        mapSettings.setCompassEnabled(true);
        mapSettings.setIndoorLevelPickerEnabled(true);
        mapSettings.setZoomControlsEnabled(true);

        map.setOnMarkerDragListener(new OnMarkerDragListener() {
            private void updateShape(@NonNull Marker marker) {
                final AreaType type = marker.getTitle().equals("Home") ? AreaType.home : AreaType.work;
                if (type == AreaType.home) {
                    homeShape.setCenter(marker.getPosition());
                }
                else {
                    workShape.setCenter(marker.getPosition());
                }
            }

            @Override
            public void onMarkerDragStart(Marker marker) {
                Log.i(tag, marker.getTitle() + " marker is now being dragged by the user.");
                updateShape(marker);
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                updateShape(marker);
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                final AreaType type = marker.getTitle().equals("Home") ? AreaType.home : AreaType.work;
                final LatLng newLocation = marker.getPosition();
                marker.remove();

                if (type == AreaType.home) {
                    homeSet = setupMarker(homeMarker, homeArea, R.id.home_button, homeShape, newLocation, type);
                    Log.i(tag, "Home marker and location updated.");
                }
                else {
                    workSet = setupMarker(workMarker, workArea, R.id.work_button, workShape, newLocation, type);
                    Log.i(tag, "Workplace marker and location updated.");
                }

                if (travelling) {
                    journeyManager(true, true, null, null);
                }

                geofencer.removeGeofences(getGeofencePendingIntent());
                setNewGeofences();
            }
        });

        // Now that we have the map fragment ready for sure, switching menus should now be possible for the user.
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switchNavItem(item);
                return true;
            }
        });

        final String userInfo = "Signed in as " + SignInActivity.username;
        final View headerView = navigationView.getHeaderView(0);
        ((TextView)headerView.findViewById(R.id.navbar_user_info)).setText(userInfo);

        // Retrieve and use their settings for the map markers/points. Don't call this
        // in `onCreate()` because it expects the map is ready to go and has been initialised.
        travelSetup();
    }
}
