package com.priority_one.tauranga_travel;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jetbrains.annotations.Contract;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// This is the main activity, it handles user access. It has been kept very simple and basic in
// appearance, as the mapping function is our main goal.
public class SignInActivity extends AppCompatActivity {
    // Change these two to suit your configuration at "app/res/values/priority_one_config.xml".
    public static String server;
    public static String path;

    // The string is used for storing data with `SharedPreferences`. Have other classes reference it.
    public static String preferencesName;
    private SharedPreferences preferences;

    // The sign-in activity must get a valid PHP session ID for location updates to work.
    // Stores the entire cookie e.g. `PHPSESSID=xyzabc`, no semicolon.
    public static String session;
    public static String username; // For displaying it on the navbar.

    // For Logcat. Can be anything.
    private static final String tag = "PriorityOneSignInLog";

    // The below code can be anything, these are just for permission callback handling.
    // They're made volatile so threads don't use a cached version or something, just to be safe.
    public static final int permissionCode = 2019;
    public static volatile Boolean permissionGranted;
    public static volatile Boolean permissionRequested = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(tag,"The application has started.");

        server = this.getString(R.string.server);
        path = this.getString(R.string.path);
        preferencesName = this.getString(R.string.preferences_name);

        setContentView(R.layout.activity_sign_in);
        preferences = getSharedPreferences(preferencesName, MODE_PRIVATE);

        // Get location permissions and handle it in another thread via callback. Meanwhile, get preferences.
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, permissionCode);

        // Android API 29 and above apparently need this permission as we are using Geofencing.
        // This permission is quite new, but it does not seem necessary if you already have COARSE and FINE
        // permissions given already. I ask the user for it again just in case, the permission callback just returns empty.
        // If this gets denied, then the above permission will be seen as denied too.
        if (android.os.Build.VERSION.SDK_INT >= 29) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_BACKGROUND_LOCATION}, permissionCode);
        }

        // Quick username autofill if we have it from previous app usage.
        final String username = preferences.getString("username", null);
        if (username != null) {
            ((EditText) findViewById(R.id.username)).setText(username);
            findViewById(R.id.password).requestFocus();
        }
    }

    // Depending on whether the user pressed the sign-up button or the sign-in button, it
    // will retrieve the credentials and send them to the server with the respective procedure.
    public void handleButtons(@NonNull final View startingView) {
        final boolean registration = startingView.getTag().toString().equals("signUp");
        Log.i(tag, "Credential handling function called.");

        if (registration) {
            userInfo("Signing up, please wait.", Color.BLACK);
        }
        else {
            userInfo("Signing in, please wait.", Color.BLACK);
        }

        final RequestQueue attemptQueue = Volley.newRequestQueue(this);

        final StringRequest attempt = new StringRequest(Request.Method.POST, server + path, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    if (response.equals("1")) {
                        Log.i(tag, "Successfully signed in or signed up.");
                        userInfo(registration ? "Successfully signed up. Please sign-in to continue." : "Successfully signed in.", Color.GREEN);

                        // Now if they're signing in, check if they can move on with correct permissions and a session.
                        if (validPermissions() && session != null && !session.isEmpty() && !registration) {
                            startTravel(); // Begin travelling.
                        }
                        else if (!registration) {
                            Log.e(tag, "No permissions or bad session.");
                            userInfo("The server cannot handle your session. Please try again or check your permissions for this app.", Color.RED);
                        }
                    }
                    else if (response.equals("0")) {
                        Log.e(tag, "Failed sign-in/sign-up attempt, bad credentials.");
                        // TODO: This needs to be further developed so the user interface properly
                        // informs the user as to what is wrong with their credentials e.g. "username already taken".
                        if (registration) {
                            userInfo("Inappropriate credentials. Please try registering again with different information.", Color.RED);
                        }
                        else {
                            userInfo("Incorrect credentials. Please try signing-in again.", Color.RED);
                        }
                    }
                    else {
                        Log.e(tag, "Failed sign-in/sign-up attempt, incorrect server response - '" + response.toString() + "'.");
                        userInfo("Server error occurred. Please try again.", Color.RED);
                    }
                }
                catch (Exception error) {
                    Log.e(tag, "Failed sign-in/sign-up attempt, unexpected server error - '" + response
                            + "'. Exception - '" + error.toString() + "'.");
                    userInfo("Server error occurred. Please try again.", Color.RED);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(tag, "Failed sign-in/sign-up attempt - '" + error.toString() + "'.");
                userInfo("Connection error occurred. Please try again.", Color.RED);
            }
        }){
            // The text fields themselves. Please don't log the latter's content.
            EditText userName = findViewById(R.id.username);
            EditText userPassword = findViewById(R.id.password);

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                if (session != null && !session.isEmpty()) {
                    return new HashMap<String, String>() {{
                        put("Cookie", session);
                    }};
                }
                else {
                    return super.getHeaders();
                }
            }

            @NonNull @Contract(" -> new") @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    //The tag in "activity_sign_in.xml" determines the operation of either "signIn" or "signUp".
                    put(startingView.getTag().toString(), ""); // No data needed.
                    put("username", userName.getText().toString());
                    put("password", userPassword.getText().toString());
                }};
            }

            @Override
            protected Response<String> parseNetworkResponse(@NonNull NetworkResponse response) {
                final Map<String, String> responseHeaders = response.headers;
                final String setCookieHeader = responseHeaders.get("Set-Cookie");

                // If you modify the regular expression, then be sure to update the group you want.
                final Pattern sessionRegex = Pattern.compile("PHPSESSID=([^;]*)");
                final Matcher sessionMatcher;

                if (setCookieHeader != null) {
                    sessionMatcher = sessionRegex.matcher(setCookieHeader);
                    final String newSession = sessionMatcher.find() ? sessionMatcher.group(0) : null;

                    if (newSession != null && newSession.isEmpty()) {
                        Log.w(tag, "Server did not give a session cookie.");
                    }
                    else {
                        Log.i(tag, "PHP session ID: " + newSession);
                        session = newSession;
                    }
                }
                else {
                    Log.i(tag, "Server sent back no Set-Cookies header, session already started.");
                }

                return super.parseNetworkResponse(response);
            }
        };

        attemptQueue.cancelAll("credentialRequest"); // Clear multiple requests.
        attempt.setTag("credentialRequest");
        attemptQueue.add(attempt);
    }

    // Just displays a message to the user. Not very pretty, but it's okay for the prototype.
    private void userInfo(String message, Integer messageColor) {
        TextView signInMessageView = findViewById(R.id.sign_in_message);
        signInMessageView.setText(message);
        signInMessageView.setTextColor(messageColor);
    }

    // Once the user has been cleared for travel, this prepares and switches to the travel activity.
    protected void startTravel() {
        // Let's save the user's time by storing their presumably now valid username.
        final SharedPreferences.Editor editPreferences = preferences.edit();
        username = ((EditText) findViewById(R.id.username)).getText().toString();

        editPreferences.putString("username", username);
        editPreferences.apply();

        Intent switchToTravel = new Intent(this, TravelActivity.class);
        Log.i(tag, "Switching the activity to 'TravelActivity'.");
        startActivity(switchToTravel);

        // Remove the login screen. Not needed anymore, I think... They can sign back in.
        finish();
    }

    // Simple permission check. TODO: Depending on the Android (API) version, this may need to be
    // fleshed out a bit more.
    public boolean validPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
            if (permissionGranted) {
                Log.i(tag, "Permission presently granted.");
            }
            else {
                Log.e(tag, "Permission not granted.");
                return false;
            }
        }
        else {
            Log.i(tag, "Permission previously granted.");
            permissionGranted = true;
            permissionRequested = true;
        }

        return true;
    }

    // A callback for when permission results are retrieved. TODO: Like the above function, this may
    // need Android API version specific conditionals depending on what the extra permission may be.
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == permissionCode) {
            Log.i(tag, "Permission results retrieved - " + String.valueOf(permissions.length) + " permissions.");
            if (permissions.length < 1) {
                Log.w(tag, "Empty permission result. Perhaps already granted.");
                return;
            }

            for (final String permission : permissions) {
                if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)){
                    // Make sure `permissionRequested` is set only after `permissionGranted` to
                    // prevent a race condition, as the former is the blocking variable.
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.i(tag, "Got accurate location permission.");
                        permissionGranted = true;
                    }
                    else {
                        Log.e(tag, "Denied accurate location permission.");
                        userInfo("You must allow the permission for fine location access to continue.", Color.RED);
                        permissionGranted = false;
                    }
                }
                else if (permission.equals(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        Log.i(tag, "Got background location permission for Geofencing.");
                        permissionGranted = true;
                    }
                    else {
                        Log.e(tag, "Denied background location permission.");
                        userInfo("Your version of Android needs background location privileges"
                                + " regardless of background activity for Geofencing.", Color.RED);
                        permissionGranted = false;
                    }
                }
                else {
                    Log.e(tag, "Got unknown permission results.");
                }

                permissionRequested = true;
            }
        }
    }
}
