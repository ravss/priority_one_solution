package com.priority_one.tauranga_travel;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.jetbrains.annotations.Contract;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.priority_one.tauranga_travel.JourneysAdapter.Journey;

public class JourneysFragment extends Fragment {
    private static final String tag = "PriorityOneJourneysLog";
    private RequestQueue journeysQueue; // TODO: Also make this into a singleton class, merging all queues.
    private Activity activity = null;
    private List<Journey> journeys;

    private RecyclerView recycler;
    private RecyclerView.Adapter adapter;
    private LinearLayoutManager layoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();

        if (activity == null) {
            Log.e(tag, "Fragment inflating in null activity. Continuing regardless.");
        }

        return inflater.inflate(R.layout.fragment_journeys, container, false);
    }

    // Treat this as the main starting point, as we now have the view.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        journeysQueue = Volley.newRequestQueue(activity);

        recycler = view.findViewById(R.id.journeys_recycler);
        recycler.setHasFixedSize(true); // Don't set this to false unless you really need it, as it otherwise lags.

        layoutManager = new LinearLayoutManager(view.getContext());
        recycler.setLayoutManager(layoutManager);

        final DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler.getContext(), layoutManager.getOrientation());
        recycler.addItemDecoration(dividerItemDecoration);
    }

    // Gets the journeys on startup.
    @Override
    public void onStart() {
        super.onStart();
        // TODO: Not sure if this should go into `onStart()` or `onResume()`.
        journeysRefresh();
    }

    // Once the fragment is not hidden anymore, it will refresh the journeys.
    @Override
    public void onHiddenChanged(final boolean hidden) {
        if (!hidden) {
            journeysRefresh();
        }
    }

    // Simply scraps the previous list of journeys and gets a fresh one. Not very efficient, but
    // proves the prototype's functionality regardless of optimisation.
    private void journeysRefresh() {
        journeys = new ArrayList<>();
        adapter = new JourneysAdapter(journeys);
        recycler.setAdapter(adapter);
        journeysRetriever();
    }

    // TODO: Another class should really be set up for Volley requests. For now, this is just a modification of a mapping request.
    private void journeysRetriever() {
        StringRequest getJourneys = new StringRequest(Request.Method.POST, SignInActivity.server + SignInActivity.path, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                if (!response.equals("0")) {
                    Log.i(tag, "Got back previous journeys.");
                    try {
                        final JSONArray parsedResponse = new JSONArray(response);
                        journeys.clear(); // Probably not needed.

                        // The encoded polylines will be retrieved individually
                        // for performance and storage reasons.
                        for (int i = 0; i < parsedResponse.length(); i++) {
                            final JSONObject parsedJourney = parsedResponse.getJSONObject(i);
                            final Long id = parsedJourney.getLong("journeyid");
                            final String startAddress = parsedJourney.getString("startaddress");
                            final String endAddress = parsedJourney.getString("endaddress");
                            final String polyline = parsedJourney.getString("encodedpolyline");
                            final Boolean completed = parsedJourney.getString("completed").equals("1");
                            journeys.add(new Journey(id, startAddress, endAddress, polyline, completed));
                        }

                        adapter.notifyDataSetChanged();
                    }
                    catch (Exception error) {
                        if (error instanceof JSONException) {
                            Log.e(tag, "JSON exception when parsing previous journeys - '" + error.getMessage() + "'.");
                        }
                        else {
                            Log.e(tag, "Could not parse returned JSON array content - '" + error.getMessage() + "'.");
                        }
                    }
                }
                else {
                    Log.i(tag, "No previous journeys.");
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // TODO: Need a much better way to deal with a network state issue.
                Log.e(tag, "Connection error detected - '" + error.getMessage() + "'.");
            }
        }){
            @NonNull @Contract(" -> new") @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    put("Cookie", SignInActivity.session);
                }};
            }

            @NonNull @Contract(" -> new") @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return new HashMap<String, String>() {{
                    put("previous", "journeys"); // This retrieves all previous journeys.
                }};
            }
        };

        journeysQueue.add(getJourneys);
    }
}
