package com.priority_one.tauranga_travel;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.PolyUtil;

import java.util.List;

import static com.google.android.gms.maps.model.JointType.ROUND;

// This class is the recycler view's adapter for the journeys fragment. It is where you would
// add more user interaction/information to the previous journeys and their details.
public class JourneysAdapter extends RecyclerView.Adapter<JourneysAdapter.JourneyViewHolder> {
    // This class has been kept small, but it can be expanded if further
    // details about a journey are needed, like e.g. time of journey beginning and end.
    public static class Journey {
        Long id;
        String startAddress;
        String endAddress;
        String polylineRoute;
        Boolean completed;

        public Journey(Long id, String startAddress, String endAddress, String polylineRoute, Boolean completed) {
            this.id = id;
            this.startAddress = startAddress;
            this.endAddress = endAddress;
            this.polylineRoute = polylineRoute;
            this.completed = completed;
        }
    }

    // Where the internal list of journey objects resides.
    private List<Journey> journeys;

    // Sets up the adapter.
    public JourneysAdapter(List<Journey> journeys) {
        this.journeys = journeys;
    }

    // This holds a single object which contains details about a journey, its embedded map, and so forth.
    public static class JourneyViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        CardView card;
        TextView addressField;
        MapView journeyMap;
        GoogleMap map;
        Polyline routeLine;
        String encodedRouteLine;

        // TODO: Each journey object/card has its own map, but that may not be efficient.
        // We could not get lite-mode to work, but ultimately this seems of little consequence.
        private JourneyViewHolder(View journeyView) {
            super(journeyView);
            card = journeyView.findViewById(R.id.journey_card);
            addressField = journeyView.findViewById(R.id.journey_address_field);
            journeyMap = journeyView.findViewById(R.id.journey_map);

            if (journeyMap != null)
            {
                journeyMap.onCreate(null);
                journeyMap.onResume();
                journeyMap.getMapAsync(this);
            }
        }

        private void drawRoute() {
            if (map == null) {
                return;
            }
            else {
                map.clear();
            }

            final LatLngBounds.Builder boundBuilder = new LatLngBounds.Builder();

            final PolylineOptions routeLineOptions = new PolylineOptions();
            routeLineOptions.color(Color.argb(255, 255, 165, 0));
            routeLineOptions.jointType(ROUND);
            routeLineOptions.width(15);
            routeLineOptions.addAll(PolyUtil.decode(encodedRouteLine));

            map.setMyLocationEnabled(false);
            map.getUiSettings().setMapToolbarEnabled(false);
            map.getUiSettings().setAllGesturesEnabled(false); // Disabled zooming etc. for now.

            if (routeLineOptions.getPoints().size() > 1) {
                for (final LatLng point : routeLineOptions.getPoints()) {
                    boundBuilder.include(point);
                }

                final LatLngBounds bounds = boundBuilder.build();
                map.setLatLngBoundsForCameraTarget(bounds);
                map.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 600, 600, 30));
                routeLine = map.addPolyline(routeLineOptions);
                map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            }
            else {
                // TODO: For now, we're just greying out a journey's map which has no "real" route.
                map.setMapType(GoogleMap.MAP_TYPE_NONE);
            }
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            MapsInitializer.initialize(itemView.getContext());
            map = googleMap;
            drawRoute();
        }
    }

    // Creates a view holder for a previous journey.
    @Override @NonNull
    public JourneysAdapter.JourneyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View journey = LayoutInflater.from(parent.getContext()).inflate(R.layout.previous_journey_recycler, parent, false);
        return new JourneyViewHolder(journey);
    }

    // Sets up the journey card by taking information from its class.
    @Override
    public void onBindViewHolder(@NonNull JourneyViewHolder holder, int i) {
        String addressLine = "";

        addressLine += journeys.get(i).startAddress.charAt(0) == '<' ? "Unknown address" : journeys.get(i).startAddress;

        // TODO: A proper drawable would be much cleaner here instead of a character symbol.
        addressLine += "  ➤  ";

        addressLine += journeys.get(i).endAddress.charAt(0) == '<' ? "Unknown address" : journeys.get(i).endAddress;

        holder.addressField.setText(addressLine);

        if (holder.routeLine != null) {
            holder.routeLine.remove();
        }

        holder.encodedRouteLine = journeys.get(i).polylineRoute;
        holder.drawRoute();
    }

    // If a journey's card goes off screen, it is recycled. This just saves some memory for now
    // by removing any polyline on a journey's map, which can be large in terms of points in them.
    @Override
    public void onViewRecycled(@NonNull JourneyViewHolder holder)
    {
        if (holder.map != null)
        {
            holder.map.clear();
            holder.map.setMapType(GoogleMap.MAP_TYPE_NONE);
        }
    }

    @Override
    public int getItemCount() {
        return journeys.size();
    }
}
