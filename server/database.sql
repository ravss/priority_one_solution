-- Database setup script intended for usage with MySQL/MariaDB.
CREATE DATABASE IF NOT EXISTS priorityone;
USE priorityone;

DROP TABLE IF EXISTS coordinates;
DROP TABLE IF EXISTS journeys;
DROP TABLE IF EXISTS users;

-- The table that contains user information.
CREATE TABLE users (
	userid BIGINT(255) NOT NULL AUTO_INCREMENT UNIQUE,
	username VARCHAR(32) NOT NULL UNIQUE,
	-- This is set to 255 characters so any future password hashing
	-- algorithms will have enough space. As of now, 100 characters is enough
	-- for Argon2id or bcrypt.
	userpass VARCHAR(255) NOT NULL,
	PRIMARY KEY (userid)
);
-- This is the table that resolves coordinates to users and vice versa.
-- It contains information related to starting and ending a journey.
CREATE TABLE journeys (
	journeyid BIGINT(255) NOT NULL AUTO_INCREMENT UNIQUE,
	userid BIGINT(255) NOT NULL,
	completed BOOLEAN NOT NULL DEFAULT FALSE,
	startaddress VARCHAR(86),
	endaddress VARCHAR(86),
	PRIMARY KEY (journeyid),
	FOREIGN KEY (userid) REFERENCES users (userid) ON DELETE CASCADE
);
-- Finally, this is the table that holds coordinate data for the journeys.
-- The journey ID is the foreign key which goes to a row in the journeys table,
-- which then ties it back to a user who created the journey.
CREATE TABLE coordinates (
	coordinateid BIGINT(255) NOT NULL AUTO_INCREMENT UNIQUE,
	journeyid BIGINT(255) NOT NULL,
	-- Latitude goes from 90 to -90 degrees, so 8 digits are needed,
	-- whereas longitude goes from 180 to -180, so 9 digits are needed.
	-- 6 decimal places are more than enough for very fine granularity,
	-- but it is up to you how accurate and precise you wish it to be.
	-- https://en.wikipedia.org/wiki/Decimal_degrees
	latitude DECIMAL(8, 6) NOT NULL,
	longitude DECIMAL(9, 6) NOT NULL,
	-- 2 decimal place accuracy for m/s, 5 digits are enough for speed.
	speed DECIMAL(5, 2) NOT NULL,
	-- Using an enumeration for movement/transport makes it independent from
	-- any algorithm specific confidence values. That is however outside our
	-- (the prototype) scope.
	movement ENUM("Stationary", "Walking", "Vehicle", "Unknown") NOT NULL DEFAULT "Unknown",
	-- Instead of using the `TIMESTAMP` type, which is limited to
	-- 32 bits and stores Unix time in seconds, Android gives back
	-- the Unix timestamp in milliseconds instead. Storing it in a 64-bit
	-- unsigned integer is just more sensible regardless of seconds.
	unixtimestampms BIGINT(255) NOT NULL,
	PRIMARY KEY (coordinateid),
	FOREIGN KEY (journeyid) REFERENCES journeys (journeyid) ON DELETE CASCADE
);
