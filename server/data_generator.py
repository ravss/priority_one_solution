#!/usr/bin/env python3
from requests import post, Session
from random import randint, choice, uniform
from time import time, sleep
from threading import Thread, Semaphore

# Configurable values.
server = "https://greatestwebsiteonearth.com:443/priorityone.php"
test_users_password = "access"
users_to_create = 20
data_points_per_user = 240

# The requests library is not good with threading. Either set a large
# enough delay or a low enough lock value on the requests library resources.
time_between_data_post = 1
lock = Semaphore(20)

# These loosely represent Tauranga, but they are not of test importance.
latitude_range = (-36, -37)
longitude_range = (176, 177)

def create_users(amount):
	users = [{
			"signUp" : "",
			"username" : "testuser" + str(i),
			"password" : test_users_password
		} for i in range(amount)
	]

	for user in users:
		response = post(server, data = user)

		if (response.content == b"0"):
			print("#", user["username"], "has travelled before.")
		else:
			print("#", user["username"], "is a new user.")

		del user["signUp"]
		user["signIn"] = ""

	return users

def send_test_data(user, driving):
	user_session = Session()

	journey_start = {
		"journey" : "start",
		"startaddress" : str(randint(1, 500)) + " Home Street",
		"endaddress" : str(randint(50, 100)) + " Work Avenue"
	}

	location_data = [{
		"location" : "",
		"latitude" : uniform(*latitude_range),
		"longitude" : uniform(*longitude_range),
		"movement" : "Vehicle" if driving else "Walking",
		"speed" : uniform(13, 60) if driving else uniform(0.5, 3),
		"unixtimestampms" : time() * 1000
		} for i in range(data_points_per_user)
	]

	# Sign-in first to obtain the PHPSESSID. The server handles the
	# current journey ID automatically. No semaphore/lock here for more
	# realism, only random sleep intervals.
	try:
		sleep(randint(1, 6))
		user_session.post(server, data = user)
		sleep(randint(1, 6))
		user_session.post(server, data = journey_start)
		sleep(randint(1, 6))
	except:
		print("!", user["username"], "failed to simulate.")
		return
	else:
		print("+", user["username"], "has begun travelling.")

	for location in location_data:
		try:
			user_session.post(server, data = location)
		except:
			print("!", user["username"], "got a connection error.")
		finally:
			sleep(time_between_data_post)

	user_session.post(server, data = {"journey" : "end"})
	print("-", user["username"], "has arrived.")
	lock.release()

def begin_multithreaded_test():
	users = create_users(users_to_create)
	user_simulation_threads = []

	for user in users:
		user_simulation = Thread(target = send_test_data,
			args = (user, choice([True, False]), ))
		user_simulation_threads.append(user_simulation)
		user_simulation.start()
		lock.acquire()

	# Wait for the threads to finish before the main thread ends.
	for simulation_thread in user_simulation_threads:
		simulation_thread.join()

begin_multithreaded_test()
