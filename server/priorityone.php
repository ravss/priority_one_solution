<?php
// This simple PHP script controls the interaction between the database and app.
// Go to the `database()` function and change its variables to reflect your configuration.
// "0" is echoed on failure and "1" is echoed on success. For the final app, it would
// be better to enhance the error messages/codes and have more functions that return
// specific journey information like time taken.

// Enable this for debugging.
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */

// Point this towards your INI configuration file that is outside the
// web document root and is not world readable from any network.
// Don't hardcode credentials here. The following string is an example path.
$secrets_file = '/srv/secrets/database_credentials.ini';

// Signs the user in and prepares session variables.
function sign_in($PDO, $username, $password) {
	$SQL = $PDO->prepare('SELECT userid, userpass FROM users WHERE username = :username');
	$SQL->execute(['username' => $username]);

	if ($SQL->rowCount() === 1) {
		$row = $SQL->fetch();

		if (password_verify($password, $row['userpass'])) {
			$_SESSION['signedIn'] = true;

			// Store anything else here like username, preferences, etc.
			$_SESSION['username'] = $username;
			$_SESSION['userid'] = $row['userid'];
			$_SESSION['journeyid'] = null;
			echo('1');
		}
		else {
			echo('0');
		}
	}
	else {
		// Raise a silent error if duplicate users somehow exist.
		echo('0');
	}
}

// Very basic sign-up function. TODO: Needs more error-checking.
function sign_up($PDO, $username, $password) {
	$hash = password_hash_wrapper($password);
	$SQL = $PDO->prepare('INSERT INTO users (username, userpass) VALUES (:username, :userpass)');

	try {
		$SQL->execute(['username' => $username, 'userpass' => $hash]);
		echo('1');
	}
	catch (Exception $ex) {
		// This often gets caught due to a username already being taken.
		// Once a more robust error system is in place, that could be better handled.
		echo('0');
		return;
	}
}

// Creates a location data row for a journey.
function location($PDO) {
	if (empty($_POST['latitude']) || empty($_POST['longitude'])
		|| empty($_POST['speed']) || empty($_POST['movement'])
		|| empty($_POST['unixtimestampms']) || empty($_SESSION['journeyid']))
	{
		echo('0');
		return;
	}
	else {
		// I think it would be wiser to have the database handle
		// the string-to-decimal conversion instead, PHP is not good at it.
		try {
			$latitude = floatval($_POST['latitude']);
			$longitude = floatval($_POST['longitude']);
		}
		catch (Exception $ex) {
			echo('0');
			return;
		}
	}

	$insert = '(journeyid, latitude, longitude, movement, speed, unixtimestampms)';
	$values = '(:journeyid, :latitude, :longitude, :movement, :speed, :unixtimestampms)';

	$SQL = $PDO->prepare("INSERT INTO coordinates $insert VALUES $values");

	$prepared = [
		'journeyid' => $_SESSION['journeyid'],
		'latitude' => $latitude,
		'longitude' => $longitude,
		'movement' => $_POST['movement'],
		'speed' => $_POST['speed'],
		'unixtimestampms' => $_POST['unixtimestampms']
	];

	try {
		$SQL->execute($prepared);
		echo('1');
	}
	catch (Exception $ex) {
		echo('0');
	}
}

// This sets up a record indicating a journey has started.
function journey_start($PDO) {
	// TODO: This entire condition block needs better checking for data validity.
	// PHP sadly only lets multiple variables into `isset()`, not `empty()`.
	if (empty($_POST['startaddress']) || empty($_POST['endaddress'])) {
		echo('0');
		return;
	}
	else {
		// May want to do some processing here for validity etc.
		// when finalising the prototype.
		$start = $_POST['startaddress'];
		$end = $_POST['endaddress'];
	}

	// Only prepare an SQL query if the data is actually valid, check prior.

	// These are just to format the code better, SQL injection is still avoided.
	$insert = '(userid, startaddress, endaddress)';
	$values = '(:userid, :startaddress, :endaddress)';
	$SQL = $PDO->prepare("INSERT INTO journeys $insert VALUES $values");

	$prepared = [
		'userid' => $_SESSION['userid'],
		'startaddress' => $start,
		'endaddress' => $end
	];

	try {
		$SQL->execute($prepared);
		echo('1');

		// No need to prepare this due to zero user input.
		$SQL = $PDO->query('SELECT LAST_INSERT_ID()');
		$_SESSION['journeyid'] = $SQL->fetchColumn();
	}
	catch (Exception $ex) {
		echo('0');
	}
}

// The opposite of `journey_start()`, but contains the same design. See above for more.
function journey_end($PDO) {
	if (empty($_SESSION['journeyid'])) {
		echo('0');
		return;
	}

	$set = 'completed = 1';
	$where = 'userid = :userid AND journeyid = :journeyid';

	$SQL = $PDO->prepare("UPDATE journeys SET $set WHERE $where");

	$prepared = [
		'userid' => $_SESSION['userid'],
		'journeyid' => $_SESSION['journeyid']
	];

	try {
		$SQL->execute($prepared);
		echo('1');
		$_SESSION['journeyid'] = null;
	}
	catch (Exception $ex) {
		echo('0');
	}
}

// TODO: Currently, this does not return the time of how long a journey
// took. It would be nice to return that value to show to the user, along with
// having a dynamic limit of how many results to return rather than just 20.
function journey_get_all($PDO) {
	$select = 'journeyid, completed, startaddress, endaddress';
	$where = 'userid = :userid';
	$order = 'journeyid DESC'; // Latest previous journeys first.
	$limit = '15'; // Only 15 journeys for now to reduce server load.

	$SQL = $PDO->prepare("SELECT $select FROM journeys WHERE $where ORDER BY $order LIMIT $limit ");

	try {
		$SQL->execute(['userid' => $_SESSION['userid']]);
		$journeys = array();

		while ($journey = $SQL->fetch()) {
			$journey['encodedpolyline'] = journey_route($PDO, $journey['journeyid']);
			$journeys[] = $journey;
		}

		echo(json_encode($journeys));
	}
	catch (Exception $ex) {
		echo('0');
	}
}

// Returns an old journey's route taken by the user. Note that this
// does not echo the encoded polyline, it returns it!
function journey_route($PDO, $id) {
	if (empty($id)) {
		return '0';
	}

	$select = 'latitude, longitude FROM coordinates';
	$where = 'journeyid';
	$idcheck = 'SELECT journeyid FROM journeys WHERE journeyid = :journeyid AND userid = :userid';
	$order = 'unixtimestampms ASC';

	$SQL = $PDO->prepare("SELECT $select WHERE $where = ($idcheck) ORDER BY $order");

	$prepared = [
		'journeyid' => $id,
		'userid' => $_SESSION['userid']
	];

	try {
		$SQL->execute($prepared);
		$route = array();

		while ($coordinate = $SQL->fetch()) {
			$route[] = $coordinate['latitude'];
			$route[] = $coordinate['longitude'];
		}

		return polylineEncode($route);
	}
	catch (Exception $ex) {
		return '0';
	}
}

// This is Google's encoded polyline format algorithm. The PolyUtil
// API can use an encoded polyline, which reduces network load, or otherwise
// we would have to send a JSON array that has all the points in it.
// https://developers.google.com/maps/documentation/utilities/polylinealgorithm
function polylineEncode($route) {
	$encoded_route = '';
	$latitudes_longitudes = array(0, 0);

	foreach ($route as $i => $point) {
		$chunks = '';

		// 1e5 precision. I don't think the algorithm works
		// with 1e6, even though we have 6 DD accuracy available.
		$point = intval(round(floatval($point) * 100000));

		$change = $point - $latitudes_longitudes[$i % 2];
		$latitudes_longitudes[$i % 2] = $point;
		$point = ($change > 0) ? ($change << 1) : ~($change << 1);

		while ($point > 0x1F) {
			$chunks .= chr(63 + (0x20 | ($point & 0x1F)));
			$point >>= 5;
		}

		$encoded_route .= $chunks . chr(63 + $point);
	}

	return $encoded_route;
}

// A wrapper function for `password_hash()` in case any options
// need to be changed later on.
function password_hash_wrapper($password) {
	// If `PASSWORD_ARGON2ID` does not work due to PHP not being compiled
	// with its algorithm, then use `PASSWORD_DEFAULT` or `PASSWORD_BCRYPT`.
	// Search for "--with-password-argon2" in `phpinfo()` if you can use it.
	// Using Argon2 over bcrypt is considered best practice for modern
	// systems if available, especially Argon2id.
	$algorithm = PASSWORD_ARGON2ID;

	return password_hash($password, $algorithm);
}

// This returns a PHP Data Object (PDO) for PDO_MYSQL.
function database() {
	global $secrets_file;
	$secrets_parsed = parse_ini_file($secrets_file);

	$username = $secrets_parsed['username'];
	$password = $secrets_parsed['password'];
	$database = $secrets_parsed['database'];
	$host = $secrets_parsed['host'];

	$DSN = "mysql:host=$host;dbname=$database;charset=utf8mb4";
	$preferences = [
		PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
	];

	try {
		return new PDO($DSN, $username, $password, $preferences);
	}
	catch (Exception $ex) {
		// Uncomment the below line if nothing works for help.
		/* echo($ex->getCode() . '\r\nDatabase failure - ' . $ex->getMessage()); */
		unset($secrets_parsed);
		return null;
	}
}

// Handles the PHP session and sets up any of its values.
function session_manager() {
	// Check if a session is started. If one hasn't then start it.
	// It will contain information related to valid login status etc.
	if (session_status() == PHP_SESSION_NONE) {
		if (session_start()) {
			if (empty($_SESSION['signedIn'])) {
				$_SESSION['signedIn'] = false;
			}

			return true;
		}
		else {
			return false;
		}
	}
}

function main() {
	$PDO = database();

	// Check for a valid database connection.
	if (is_null($PDO)) {
		// Let the app handle the error as a server-side error, as
		// we should hide the actual error outside of testing purposes.
		http_response_code(500);
		return;
	}
	else {
		if (!session_manager() || isset($_POST['signOut'])) {
			session_destroy();
			return;
		}
	}

	if (!empty($_POST['username']) && !empty($_POST['password'])) {
		if (isset($_POST['signIn'])) {
			sign_in($PDO, $_POST['username'], $_POST['password']);
		}
		else if (isset($_POST['signUp'])) {
			sign_up($PDO, $_POST['username'], $_POST['password']);
		}
		else {
			echo('0');
			return;
		}
	}
	else if (!$_SESSION['signedIn']) {
		// Don't go any forward if they have no signed in.
		http_response_code(401);
		return;
	}

	// Checking for their sign-in status is not needed below, but I am
	// going to continue doing it just for complete clarity.

	if (isset($_POST['location']) && $_SESSION['signedIn']) {
		location($PDO);
	}
	else if (isset($_POST['journey']) && $_SESSION['signedIn']) {
		if ($_POST['journey'] === 'start') {
			journey_start($PDO);
		}
		else if ($_POST['journey'] === 'end') {
			journey_end($PDO);
		}
		else if ($_POST['journey'] === 'cancel') {
			// Alternative to marking the journey as completed.
			$_SESSION['journeyid'] = null;
		}
		else {
			echo('0');
			return;
		}
	}
	else if (isset($_POST['previous']) && $_SESSION['signedIn']) {
		if ($_POST['previous'] === 'journeys') {
			journey_get_all($PDO);
		}
		else if ($_POST['previous'] === 'route') {
			echo(journey_route($PDO, $_POST['journeyid']));
		}
	}
} main();
?>